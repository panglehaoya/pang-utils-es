# 项目简介

使用ts封装项目中常用的utils工具函数

# 安装与使用

```text
npm install pang-utils-es
```

在项目中可按需导入

```js
import { debounce } from 'pang-utils-es'
```

## websocket

### 事件Event
```js
import { Event } from 'pang-utils-es'

const event = new Event()
event.on('test', handleTest)
event.emit('test', args)

function handleTest(val) {}

// 清空特定事件
event.off('test', handleTest)
// 清空所有事件
event.clear()
```

### Websocket
```vue
import {eventBus, WebsocketClass} from 'pang-utils-es'

<script>
import Vue from "vue";
import { WebsocketClass, eventBus } from "pang-websocket";

export default Vue.extend({
  name: "Index",
  mounted() {
    const ws = new WebsocketClass(
      "ws://localhost:3001/websocket/1",
      {reConnectFn: this.handleReconnect}
    );
    eventBus.on("ws-open", this.handleWSOpen);
    // 超过连接次数还未连接成功时触发该事件
    eventBus.on("ws-close", this.handleWSClose);
    ws.init();
    // 如果不需要响应式 可以不放在data中
    this.ws = ws;
  },
  methods: {
    // 重连时的回调处理 需要返回promise resolve后 才会进行下一次重连
    handleReconnect(times, e) {
      return new Promise((resolve) => {
        setTimeout(() => {
          console.log("reconnect", times, e);
          resolve("done");
        }, 2000);
      });
    },
    handleWSOpen() {
      this.ws.sendMsg("text", { name: "name" }).then((res) => {
        console.log("handle res", res);
      });
    },
    handleClick() {
      this.ws.sendMsg("other", { other: "other" }).then((res) => {
        console.log("handle other", res);
      });
    },
    // 主动关闭链接
    handleActiveClose() {
      this.ws.closeWS().then((res) => {
        console.log("close ws", res);
      });
    },
    handleWSClose() {
      console.log("reconnect fail close ws");
    },
  },
});
</script>
```


## common

### deepClone

```text
不考虑边界情况的深拷贝，用于简单的对象
```

```js

import { deepClone } from 'pang-utils-es'

const obj = { name: 'a' }
const deepCloneObj = deepClone(obj)

deepClone.name = 'b'
console.log(obj.name) // 'a'
```

### uniqueArr

```text
使用set对数组去重
```

```js
import { uniqueArr } from 'pang-utils-es'

const arr = [1,2,2,3,1]
console.log(uniqueArr(arr)) // [1,2,3]
```

### getUUID

```text
获取uuid
```

```js
import { getUUID } from 'pang-utils-es'

// 可以指定长度
const uuid1 = getUUID(10)

// 设置基准值
const uuid2 = getUUID(10, 10)
```

### formatTime

```text
 格式化时间
```

```js
import { formatTime } from 'pang-utils-es'

console.log(formatTime(new Date())) //2023-11-12 12:00:00

console.log(formatTime(new Date(), '{y}-{m}-{d}')) // 2023-11-12

console.log(formatTime(new Date(), '{h}-{i}-{s}')) // 12:00:00
```

### listToTree

```text
列表转换为树结构
```

```js
import { listToTree } from 'pang-utils-es'

const list = [
  {
    id: 1001,
    parentId: 0,
    name: "AA",
  },
  {
    id: 1002,
    parentId: 1001,
    name: "BB",
  },
  {
    id: 1003,
    parentId: 1001,
    name: "CC",
  },
];
const treeData = listToTree(list)
console.data(treeData) 
// [
// {
//   id: 1001,
//     parentId: 0,
//   name: "AA",
//   children: [
//   {
//     id: 1002,
//     parentId: 1001,
//     name: "BB",
//   },
//   {
//     id: 1003,
//     parentId: 1001,
//     name: "CC",
//   },
// ],
// },
// ];
```

### pick

```text
选择对象的某些key，返回一个新的对象
```

```js
import {pick} from 'pang-utils-es'

const obj = {name: 'a', type: 'form', data: 'data'}
const pickObj = pick(obj, ['name', type])

console.log(pickObj) // {name: 'a', type: 'form'}
```

## url

### parseUrl

```text
解析url中的查询参数
```

```js
import {parseUrl} from 'pang-utils-es'

const obj = parseUrl('?data=a&type=form')
console.log(obj) // {data: 'a', type: 'form'}
```

### serialize 

```text
序列化对象为查询参数
```

```js
import {serialize} from 'pang-utils-es'

const obj = {data: 'a', type: 'form'}
const str = serialize(obj) //?data=a&type=form
```

## valid

### isNull

```text
判断值是否为null
```

```js
import {isNull} from 'pang-utils-es'

console.log(isNull(0)) // false 
console.log(isNull('')) // false
console.log(isNull(null)) // true
```
### isUndefined

```text
判断值是否为undefined
```
```js
import {isUndefined} from 'pang-utils-es'

console.log(isUndefined(0)) //false
console.log(isUndefined()) //true
```
### isObject

```text
判断值是否为对象
```

```js
import {isObject} from 'pang-utils-es'

isObject(null) // false
isObject([]) // true
isObject({}) // true
isObject(123) // false
```
### isEmptyObj

```text
判断是否为空对象
```

```js
import {isEmptyObj} from 'pang-utils-es'

isEmptyObj({}) // true
isEmptyObj({name: 'a'}) // false
```

### isEqualObj

```text
判断是否为两个相等的对象
```

```js
import {isEqualObj} from 'pang-utils-es'

isEqualObj({}, {}) // false
isEqualObj({ name: "a" }, { name: "a" }) // true
```
