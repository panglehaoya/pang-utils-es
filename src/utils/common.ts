import { isEmptyObj } from "@/utils/valid";

/**
 * This valid just a simple version of deep copy
 * Has a lot of edge cases bug
 * If you want to use a perfect deep copy, use lodash's _.cloneDeep
 * @param {Object} source
 * @returns {Object}
 */
export function deepClone(source: Record<any, any>): Record<any, any> {
  if (!source && typeof source !== "object") {
    throw new Error("source should be object");
  }
  const targetObj = source.constructor === Array ? [] : {};
  Object.keys(source).forEach((keys) => {
    if (source[keys] && typeof source[keys] === "object") {
      targetObj[keys] = deepClone(source[keys]);
    } else {
      targetObj[keys] = source[keys];
    }
  });
  return targetObj;
}

/**
 * uniqueArr
 * @param {Array} arr
 * @returns {Array}
 */
export function uniqueArr(arr: Array<any>) {
  return Array.from(new Set(arr));
}

/**
 * 获取uuid
 * @param {number} len uuid长度
 * @param {number} radix 基数
 */
export function getUUID(len?: number, radix?: number) {
  const chars =
    "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".split("");
  const uuid = [];
  let i;
  radix = radix || chars.length;
  if (len) {
    for (i = 0; i < len; i++) uuid[i] = chars[0 | (Math.random() * radix)];
  } else {
    let r;
    uuid[8] = uuid[13] = uuid[18] = uuid[23] = "-";
    uuid[14] = "4";
    for (i = 0; i < 36; i++) {
      if (!uuid[i]) {
        r = 0 | (Math.random() * 16);
        uuid[i] = chars[i == 19 ? (r & 0x3) | 0x8 : r];
      }
    }
  }
  return uuid.join("");
}

/**
 * format the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string | null}
 */
export function formatTime(time: Date | string | number, cFormat?: string) {
  if (arguments.length === 0 || !time) {
    return null;
  }
  const format = cFormat || "{y}-{m}-{d} {h}:{i}:{s}";
  let date;
  if (typeof time === "object") {
    date = time;
  } else {
    if (typeof time === "string") {
      if (/^[0-9]+$/.test(time)) {
        // support "1548221490638"
        time = parseInt(time);
      } else {
        // support safari
        // https://stackoverflow.com/questions/4310953/invalid-date-in-safari
        time = time.replace(new RegExp(/-/gm), "/");
      }
    }

    if (typeof time === "number" && time.toString().length === 10) {
      time = time * 1000;
    }
    date = new Date(time);
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay(),
  };

  return format.replace(/{([ymdhisa])+}/g, (result, key) => {
    const value = formatObj[key];
    // Note: getDay() returns 0 on Sunday
    if (key === "a") {
      return ["日", "一", "二", "三", "四", "五", "六"][value];
    }
    return value.toString().padStart(2, "0");
  });
}

/**
 * 列表转换为树结构
 * @param {Array} data
 * @param {{id: string,parentId: string}} option
 */
export function listToTree(data, option = { id: "id", parentId: "parentId" }) {
  const obj = {};
  const idKey = option ? option.id : "id";
  const parentKey = option ? option.parentId : "parentId";

  data.forEach((item) => {
    obj[item[idKey]] = item;
  });
  const parentList = [];
  data.forEach((item) => {
    const parent = obj[item[parentKey]];
    if (parent) {
      parent.children = parent.children || [];
      parent.children.push(item);
    } else {
      parentList.push(item);
    }
  });

  return parentList;
}

/**
 * 根据 keys 过滤指定的属性值，返回一个新的对象
 * @param {Record<any, any>} obj
 * @param {Array<string>} keys
 * @return {Record<any, any>} obj
 */
export function pick(obj: Record<any, any>, keys: Array<string>) {
  if (isEmptyObj(obj)) return obj;
  if (!keys.length) return obj;
  const newObj = {};

  keys.forEach((k) => {
    newObj[k] = obj[k];
  });

  return newObj;
}
