type THandler<T = any> = (event: T) => any;

export class EventBus {
  private map = new Map<string, Array<THandler>>();

  on(type: string, handler: THandler) {
    if (!this.map.has(type)) {
      this.map.set(type, [handler]);
    } else {
      const handlers = this.map.get(type);
      handlers.push(handler);
      this.map.set(type, handlers);
    }
  }

  emit(type: string, event?: unknown) {
    const handlers = this.map.get(type);
    if (handlers) {
      handlers.forEach((h) => {
        h(event);
      });
    }
  }

  once(type: string, handler: THandler) {
    const temp = (...args) => {
      handler.apply(this, args as any);
      this.off(type, temp);
    };

    this.on(type, temp);
  }

  off(type: string, handler?: THandler) {
    const handlers = this.map.get(type);
    if (handlers) {
      if (handler) {
        const index = handlers.findIndex((h) => h === handler);
        if (index !== -1) {
          handlers.splice(index, 1);
        }
      } else {
        this.map.set(type, []);
      }
    }
  }

  clear() {
    this.map.clear();
  }
}
