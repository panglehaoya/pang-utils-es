/**
 * 解析url
 * @param {string} url = window.location.search
 * @return {Record<string, string>>}
 */
export function parseUrl(url = window.location.search): Record<any, any> {
  const searchParams = new URLSearchParams(url);
  const obj = {};
  for (const p of searchParams) {
    obj[p[0]] = p[1];
  }

  return obj;
}

/**
 * 将对象转换为查询参数
 * @param {Record<any, any>>} obj
 * @return {Boolean}
 */
export function serialize(obj: Record<any, any>) {
  let str = "";
  const keys = Object.keys(obj);
  keys.forEach((k) => {
    str += `${k}=${obj[k]}&`;
  });

  return `?${str.slice(0, str.length - 1)}`;
}
