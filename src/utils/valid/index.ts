/**
 * 判断是否为Null
 * @param {Record<any, any>} val
 * @return {Boolean}
 */
export function isNull(val: any): boolean {
  return val === null;
}

/**
 * 判断值是否为undefined
 * @param val
 * @return {Boolean}
 */
export function isUndefined(val?: any) {
  return typeof val === "undefined";
}

/**
 * 判断是否为对象
 * @param value
 * @return {Boolean}
 */
export function isObject(value: any) {
  if (value === null) return false;
  return typeof value === "object";
}

/**
 * 判断是否空对象
 * @param {Record<any, any>>} value
 * @return {Boolean}
 */
export function isEmptyObj(value: Record<any, any>) {
  if (!isObject(value)) return false;
  const keys = Object.keys(value);
  return !keys.length;
}

/**
 * 判断两个对象是否相等
 * @param {Record<any, any>>}  obj1
 * @param {Record<any, any>>}  obj2
 * @return {Boolean}
 */
export function isEqualObj(obj1: Record<any, any>, obj2: Record<any, any>) {
  if (!isObject(obj1) || !isObject(obj2)) {
    return obj1 === obj2;
  }
  if (obj1 === obj2) return true;

  const obj1Length = Object.keys(obj1).length;
  const obj2Length = Object.keys(obj2).length;
  if (obj1Length !== obj2Length) return false;

  return Object.keys(obj1).some((key) => isEqualObj(obj1[key], obj2[key]));
}
