import {
  deepClone,
  formatTime,
  getUUID,
  uniqueArr,
  listToTree,
  pick,
} from "@/utils/common";

describe("common utils", () => {
  it("deepClone", () => {
    const obj = { name: "a", type: { name: "form" }, list: [1, 2, 3] };
    const cloneObj = deepClone(obj);
    cloneObj.type.name = "form1";
    cloneObj.list.push(4);

    expect(obj.type.name).toEqual("form");
    expect(obj.list[3]).toBeUndefined();
  });

  it("uniqueArr", () => {
    const arr = [1, 2, 1, 2, 3];

    expect(uniqueArr(arr)).toEqual([1, 2, 3]);
  });

  it("getUUID", () => {
    const uuid = getUUID(10);

    expect(uuid.length).toEqual(10);
  });

  it("formatTime", () => {
    const time = formatTime(new Date(), "{y}-{m}-{d}");
    const date = new Date();
    const currentTime = `${date.getFullYear()}-${
      date.getMonth() + 1
    }-${date.getDate()}`;

    expect(time).toEqual(currentTime);
  });

  it("listToTree", () => {
    const list = [
      {
        id: 1001,
        parentId: 0,
        name: "AA",
      },
      {
        id: 1002,
        parentId: 1001,
        name: "BB",
      },
      {
        id: 1003,
        parentId: 1001,
        name: "CC",
      },
      {
        id: 1004,
        parentId: 1003,
        name: "DD",
      },
      {
        id: 1005,
        parentId: 1003,
        name: "EE",
      },
      {
        id: 1006,
        parentId: 1002,
        name: "FF",
      },
      {
        id: 1007,
        parentId: 1002,
        name: "GG",
      },
      {
        id: 1008,
        parentId: 1004,
        name: "HH",
      },
      {
        id: 1009,
        parentId: 1005,
        name: "II",
      },
    ];
    const tree = listToTree(list);

    expect(tree[0].id).toEqual(1001);
  });

  it("pick", () => {
    const obj = { name: "a", type: "form" };
    const objPick = pick(obj, ["name"]);

    // eslint-disable-next-line no-prototype-builtins
    expect(objPick.hasOwnProperty("name")).toEqual(true);
    // eslint-disable-next-line no-prototype-builtins
    expect(objPick.hasOwnProperty("type")).toEqual(false);
  });
});
