import { EventBus } from "@/utils/event";
const eventBus = new EventBus();

describe("EventBus", () => {
  afterEach(() => {
    eventBus.clear();
  });

  it("should on", () => {
    const mockFn = jest.fn();
    eventBus.on("message", mockFn);
    eventBus.emit("message");

    expect(mockFn).toBeCalledTimes(1);
  });

  it("show on with params", () => {
    const mockFn = jest.fn();
    eventBus.on("message", () => {
      mockFn("message");
    });
    eventBus.emit("message");

    expect(mockFn).toHaveBeenCalledWith("message");
  });

  it("should emit", () => {
    const mockFn = jest.fn();
    eventBus.on("message", mockFn);
    eventBus.emit("message");
    eventBus.emit("message");

    expect(mockFn).toBeCalledTimes(2);
  });

  it("should once", () => {
    const mockFn = jest.fn();
    eventBus.once("message", mockFn);
    eventBus.emit("message");
    eventBus.emit("message");

    expect(mockFn).toBeCalledTimes(1);
  });

  it("should off", () => {
    const mockFn = jest.fn();
    eventBus.on("message", mockFn);
    eventBus.emit("message");
    eventBus.off("message", mockFn);
    eventBus.emit("message");

    expect(mockFn).toBeCalledTimes(1);
  });

  it("should clear", () => {
    const mockFn = jest.fn();
    eventBus.on("message", mockFn);
    eventBus.emit("message");
    eventBus.clear();
    eventBus.emit("message");

    expect(mockFn).toBeCalledTimes(1);
  });
});
