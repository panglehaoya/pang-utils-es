import { parseUrl, serialize } from "@/utils/url";

describe("url utils", () => {
  it("parseUrl should return obj", () => {
    const url = "?data=a&type=form";
    const obj = parseUrl(url);

    expect(Object.keys(obj).length).toEqual(2);
  });

  it("parseUrl should have correct value", () => {
    const url = "?data=a&type=form";
    const obj = parseUrl(url);
    const value1 = obj.data;
    const value2 = obj.type;

    expect(value1).toEqual("a");
    expect(value2).toEqual("form");
  });

  it("serialize should have correct value", () => {
    const obj = { name: "a", type: "form" };
    const url = serialize(obj);

    expect(url).toEqual("?name=a&type=form");
  });
});
