import {
  isEmptyObj,
  isNull,
  isObject,
  isUndefined,
  isEqualObj,
} from "@/utils/valid";

describe("valid", () => {
  it("isNull", () => {
    expect(isNull(0)).toEqual(false);
    expect(isNull("")).toEqual(false);
    expect(isNull(null)).toEqual(true);
  });

  it("isUndefined", () => {
    let a;
    const res = isUndefined(a);

    expect(res).toEqual(true);
  });

  it("is object", () => {
    const obj = {};
    const res = isObject(obj);

    expect(res).toEqual(true);
  });

  it("null is not object", () => {
    const obj = null;
    const res = isObject(obj);

    expect(res).toEqual(false);
  });

  it("isEmptyObject", () => {
    const obj = {};
    const obj1 = { name: "a" };
    const res = isEmptyObj(obj);
    const res1 = isEmptyObj(obj1);

    expect(res).toEqual(true);
    expect(res1).toEqual(false);
  });

  it("isEqualObject", () => {
    const obj1 = { name: "a" };
    const obj2 = { name: "a" };
    const res = isEqualObj(obj1, obj2);

    expect(res).toEqual(true);
  });
});
