const Koa = require("koa");
const Router = require("koa-router");
const websocket = require("koa-websocket");

const app = websocket(new Koa());
const router = new Router();

app.ws.use((ctx, next) => {
  return next(ctx);
});

router.get("/", async (ctx) => {
  ctx.body = "hello koa";
});

router.all("/ws", async (ctx) => {
  ctx.websocket.send("server message");

  ctx.websocket.on("message", (msg) => {
    console.log("前端发过来的数据：", msg);
  });
  ctx.websocket.on("close", () => {
    console.log("前端关闭了websocket");
  });
});

app.ws.use(router.routes()).use(router.allowedMethods());

app.listen(3000, () => {
  console.log("koa is listening in 3000");
});
